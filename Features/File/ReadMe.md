# Files

This section contains useful snippets dealing with files and file IO within FileMaker.

- File Preview: a recipe for previewing files.
- Insert File: a standalone script for use on a OnObjectEnter for container fields.